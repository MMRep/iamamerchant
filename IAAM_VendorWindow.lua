local _, IAAM = ...

--============================================================
--============================================================
--
--             Variables
--

IAAM.VW                 = {}
IAAM.VW.ITEMS_PER_PAGE  = 12
IAAM.VW.UPDATE_INTERVAL = 1.0

IAAM.VW.ItemButton        = {}
IAAM.VW.NextPageButton    = {}
IAAM.VW.PrevPageButton    = {}
IAAM.VW.AddItemPopup      = {}
IAAM.VW.AddItemPopup.Link = nil

--============================================================
--============================================================
--
--             Global functions
--

--============================================================
--
--             IAAM.VW.GetItemsForSaleTable()
--
function IAAM.VW.GetItemsForSaleTable()
    local Table
    if IAAM_VW.FontStringTitle:GetText() == IAAM.CORE_GetName("player") then
        Table = IAAM_Character.ItemsForSale
    else
        Table = IAAM.Merchant.ItemsForSale
    end
    return Table
end

--============================================================
--
--             IAAM.VW.GetPageString()
--
function IAAM.VW.GetPageString(Merchant)
    return "Page "..IAAM_VW.Page.." of "..math.ceil(IAAM.CORE_GetNumItems(IAAM.VW.GetItemsForSaleTable()) / IAAM.VW.ITEMS_PER_PAGE)
end

--============================================================
--
--             IAAM.VW.NextPageButton:OnClick()
--
function IAAM.VW.NextPageButton:OnClick()
    IAAM.DEBUG.Print(false, "IAAM_VW.NextPageButton.OnClick()")
    if IAAM_VW.Page < (IAAM.CORE_GetNumItems(IAAM.VW.GetItemsForSaleTable()) / IAAM.VW.ITEMS_PER_PAGE) then
        IAAM_VW.Page = IAAM_VW.Page + 1
        IAAM_VW.FontStringPage:SetText(IAAM.VW.GetPageString())
        IAAM.VW.Update()
    end
end

--============================================================
--
--             IAAM.VW.PrevPageButton:OnClick()
--
function IAAM.VW.PrevPageButton:OnClick()
    IAAM.DEBUG.Print(false, "IAAM_VW.PrevPageButton.OnClick()")
    if IAAM_VW.Page > 1 then
        IAAM_VW.Page = IAAM_VW.Page - 1
        IAAM_VW.FontStringPage:SetText(IAAM.VW.GetPageString())
        IAAM.VW.Update()
    end
end

--============================================================
--
--             IAAM.VW.ItemButton.OnClick()
--
function IAAM.VW.ItemButton.OnClick(self, button, down)
    IAAM.DEBUG.Print(false, "IAAM.VW.ItemButton.OnClick()")
    if button == "RightButton" then
        if IAAM_VW.Name == IAAM.CORE_GetName("player") then
            --
            -- Remove item from your list
            --
            IAAM_Character.ItemsForSale[""..self.link] = nil
            IAAM.VW.Update()
        end
    elseif IsShiftKeyDown() and button == "LeftButton" then
        DEFAULT_CHAT_FRAME.editBox:Insert(self.link)
    end
end

--============================================================
--
--             IAAM.VW.AddItemPopup.OnAccept()
--
function IAAM.VW.AddItemPopup.OnAccept(self)
    local StackSize = tonumber(IAAM_PopupExtension.EditBox1:GetText())
    local StackPrice = tonumber(IAAM_PopupExtension.EditBox2:GetText())
    if StackPrice == nil then
        StackPrice = 0
    end
IAAM.CORE_AddItem(IAAM_Character.ItemsForSale, IAAM.VW.AddItemPopup.Link, StackSize, StackPrice, C_Item.GetItemCount(IAAM.VW.AddItemPopup.Link, true, true))
    IAAM.VW.Update()
end

--============================================================
--
--             IAAM.VW.AddItemPopup.OnShow()
--
function IAAM.VW.AddItemPopup.OnShow(self)
    IAAM.VW.AddItemPopup.Frame = self
    IAAM_PopupExtension.EditBox1:SetText("1")
    IAAM_PopupExtension.EditBox2:SetScript("OnEnterPressed", IAAM.VW.AddItemPopup.OnEnterPressed)
    IAAM_PopupExtension.EditBox2:SetScript("OnEscapePressed", IAAM.VW.AddItemPopup.OnEscapePressed)
    IAAM_PopupExtension.EditBox1:SetScript("OnTabPressed", function ()
        IAAM_PopupExtension.EditBox2:SetFocus()
    end)
    IAAM_PopupExtension.EditBox2:SetScript("OnTabPressed", function ()
        IAAM_PopupExtension.EditBox1:SetFocus()
    end)
    IAAM_PopupExtension.EditBox2:SetScript("OnTextChanged", function ()
        local Money = tonumber(IAAM_PopupExtension.EditBox2:GetText()) or 0
        MoneyFrame_Update(IAAM.VW.AddItemPopup.Frame.moneyFrame, Money)
    end)
    IAAM_PopupExtension.EditBox2:SetFocus()
end

--============================================================
--
--             IAAM.VW.AddItemPopup.OnEnterPressed()
--
function IAAM.VW.AddItemPopup.OnEnterPressed(self)
    IAAM.VW.AddItemPopup.OnAccept(IAAM.VW.AddItemPopup.Frame)
    IAAM.VW.AddItemPopup.Frame:Hide()
end

--============================================================
--
--             IAAM.VW.AddItemPopup.OnEscapePressed()
--
function IAAM.VW.AddItemPopup.OnEscapePressed(self)
    IAAM.VW.AddItemPopup.Frame:Hide()
end

--============================================================
--
--             IAAM.VW.OnReceiveDrag()
--
function IAAM.VW.OnReceiveDrag(self)
    local Type, ItemID, ItemLink = GetCursorInfo()
    ClearCursor()
    if IAAM.CORE_GetName("player") == IAAM_VW.FontStringTitle:GetText() then
        if Type == "item" then
            IAAM.VW.AddItemPopup.Link = ItemLink
            local ItemName, _, ItemQuality = C_Item.GetItemInfo(ItemLink)
            r, g, b = C_Item.GetItemQualityColor(ItemQuality)
            ItemTexture = select(10, C_Item.GetItemInfo(ItemLink))
            local Data = {
                texture = ItemTexture,
                name    = ItemName,
                color   = {r, g, b, 1},
                link    = ItemLink,
                index   = ItemID
            }
            StaticPopup_Show("IAAM.VW.AddItemPopup", nil, nil, Data, IAAM_PopupExtension)
        end
    end
end

--============================================================
--
--             IAAM.VW.OnMouseDown()
--
function IAAM.VW.OnMouseDown(self, button)
    if button == "LeftButton" then
        IAAM.VW.OnReceiveDrag(nil)
    end
end

--============================================================
--
--             IAAM.VW.OnHide()
--
function IAAM.VW.OnHide(self)
    IAAM.DEBUG.Print(false, "IAAM.VW.OnHide()")
    PlaySound(SOUNDKIT.IG_CHARACTER_INFO_CLOSE)
end

--============================================================
--
--             IAAM.VW.OnLoad()
--
function IAAM.VW.OnLoad(self)
    IAAM.DEBUG.Print(false, "IAAM.VW.OnLoad()")
    IAAM_VW.Page = 1
    --
    -- Make window movable
    --
    self:RegisterForDrag("LeftButton")
    --
    -- Set up item buttons
    --
    for i = 1, IAAM.VW.ITEMS_PER_PAGE do
        local ItemButton = IAAM_VW["FrameItem"..i].ItemButton
        ItemButton:SetScript("OnClick", IAAM.VW.ItemButton.OnClick)
        ItemButton:SetScript("OnEnter", IAAM.VW.ItemButton.OnEnter)
        ItemButton:SetScript("OnLeave", IAAM.VW.ItemButton.OnLeave)
        ItemButton.UpdateTooltip = IAAM.VW.ItemButton.OnEnter
    end
    --
    -- Set scripts
    --
    IAAM_VW:SetScript("OnHide", IAAM.VW.OnHide)
    IAAM_VW:SetScript("OnShow", IAAM.VW.OnShow)
    IAAM_VW:SetScript("OnMouseDown", IAAM.VW.OnMouseDown)
    IAAM_VW:SetScript("OnMouseWheel", IAAM.VW.OnMouseWheel)
    IAAM_VW:SetScript("OnReceiveDrag", IAAM.VW.OnReceiveDrag)
    IAAM_VW.NextPageButton:SetScript("OnClick", IAAM.VW.NextPageButton.OnClick)
    IAAM_VW.PrevPageButton:SetScript("OnClick", IAAM.VW.PrevPageButton.OnClick)
end

--============================================================
--
--             IAAM.VW.HideItems()
--
function IAAM.VW.HideItems()
    IAAM.DEBUG.Print(false, "IAAM.VW.HideItems()")
    for i = 1, IAAM.VW.ITEMS_PER_PAGE do
        IAAM_VW["FrameItem"..i]:Hide()
    end
end

--============================================================
--
--             IAAM.VW.OnShow()
--
function IAAM.VW.OnShow(self)
    IAAM.DEBUG.Print(false, "IAAM.VW.OnShow()")
    PlaySound(SOUNDKIT.IG_CHARACTER_INFO_OPEN)
    local Name = self.Name
    if Name == nil then
        self:Hide()
    else
        IAAM_VW.FontStringTitle:SetText(Name)
        if Name == IAAM.CORE_GetName("player") then
            SetPortraitTexture(IAAM_VWPortrait, "player")
        else
            SetPortraitTexture(IAAM_VWPortrait, Name)
        end
        _, TextureType = GetTextureInfo(IAAM_VWPortrait)
        if TextureType == "Unknown" then
            ButtonFrameTemplate_HidePortrait(IAAM_VW)
        else
            ButtonFrameTemplate_ShowPortrait(IAAM_VW)
        end
    end
    IAAM_VW.FontStringPage:SetText(IAAM.VW.GetPageString())
    IAAM.VW.Update()
end

--============================================================
--
--             IAAM.VW.OnMouseWheel()
--
function IAAM.VW.OnMouseWheel(self, value)
    IAAM.DEBUG.Print(false, "IAAM.VW.OnMouseWheel()")
  if value > 0 then
    if IAAM_VW.PrevPageButton:IsShown() and IAAM_VW.PrevPageButton:IsEnabled() then
      IAAM.VW.PrevPageButton.OnClick(IAAM_VW.PrevPageButton)
    end
  else
    if IAAM_VW.NextPageButton:IsShown() and IAAM_VW.NextPageButton:IsEnabled() then
        IAAM.VW.NextPageButton.OnClick(IAAM_VW.NextPageButton)
    end
  end
end

--============================================================
--
--             IAAM.VW.ItemButton.OnEnter()
--
function IAAM.VW.ItemButton.OnEnter(self)
    IAAM.DEBUG.Print(false, "IAAM.VW.ItemButton.OnEnter()")
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetMerchantItem(self:GetID())
    GameTooltip_ShowCompareItem(GameTooltip)
    GameTooltip:SetHyperlink(self.link)
end

--============================================================
--
--             IAAM.VW.ItemButton.OnLeave()
--
function IAAM.VW.ItemButton.OnLeave(self)
    IAAM.DEBUG.Print(false, "IAAM.VW.ItemButton.OnLeave()")
    GameTooltip:Hide()
end

--============================================================
--
--             IAAM.VW.Open()
--
function IAAM.VW.Open(Name)
    IAAM.DEBUG.Print(false, "IAAM.VW.Open()")
    -- Close merchant window if open
    if IAAM_VW:IsShown() == true then
        IAAM_VW:Hide()
    end
    -- If name valid, open new merchant window, no matter if it's the same player
    if IAAM.CORE_IsValidName(Name) then
        IAAM.Merchant.ItemsForSale = {}
        IAAM_VW.Name = Name
        IAAM.COM_SendRequestItems(Name)
    end
end

--============================================================
--
--             IAAM.VW.Toggle()
--
function IAAM.VW.Toggle(Name)
    IAAM.DEBUG.Print(false, "IAAM.VW.Toggle()")
    if IAAM_VW:IsShown() and IAAM_VW.FontStringTitle:GetText() == IAAM.CORE_GetName("player") then
        IAAM_VW:Hide()
    else
        IAAM_VW:Hide()
        IAAM.CORE_UpdateList()
        IAAM_VW.Name = Name
        IAAM_VW:Show()
    end
end

--============================================================
--
--             IAAM.VW.Update()
--
function IAAM.VW.Update()
    ItemsForSale = IAAM.VW.GetItemsForSaleTable()
    IAAM.DEBUG.Print(false, "IAAM.VW.Update()")

    local NumItems = IAAM.CORE_GetNumItems(ItemsForSale)

    IAAM_VW.FontStringPage:SetText(IAAM.VW.GetPageString())

    IAAM.VW.HideItems()
    for i = 1, IAAM.VW.ITEMS_PER_PAGE do
        local Index = (((IAAM_VW.Page - 1) * IAAM.VW.ITEMS_PER_PAGE) + i)
        if Index <= NumItems then
            local Item            = IAAM.CORE_GetItemAt(ItemsForSale, Index)
            local ItemFrame       = IAAM_VW["FrameItem"..i]
            local ItemFrameButton = IAAM_VW["FrameItem"..i].ItemButton
            local ItemFrameMoney  = _G["IAAM_VWFrameItem"..i.."MoneyFrame"] -- We need to access a global name, since Blizzard isn't specifying parentKey as they did for ItemButton and other.
            local ItemFrameName   = IAAM_VW["FrameItem"..i].Name
            IAAM.CORE_QueryItemInfo(Item)
            ItemFrameName:SetText(Item.Name)
            SetItemButtonCount(ItemFrameButton, Item.StackSize)
            SetItemButtonStock(ItemFrameButton, Item.Quantity)
            SetItemButtonTexture(ItemFrameButton, Item.Texture)
            ItemFrameButton.price        = Item.Price
            ItemFrameButton.extendedCost = nil
            ItemFrameButton.name         = Item.Name
            ItemFrameButton.link         = Item.Link
            ItemFrameButton.ItemID       = Item.ID
            ItemFrameButton.texture      = Item.Texture
            MoneyFrame_SetMaxDisplayWidth(ItemFrameMoney, IAAM_MAX_MONEY_DISPLAY_WIDTH)
            MoneyFrame_Update(ItemFrameMoney, Item.Price)
            SetMoneyFrameColor(ItemFrameMoney:GetName(), nil)
            MerchantFrameItem_UpdateQuality(ItemFrame, Item.Link)
            ItemFrameButton.showNonrefundablePrompt = false
            ItemFrameButton.hasItem = true
            ItemFrameButton:SetID(Index)
            ItemFrame:Show()
        end
    end
end

StaticPopupDialogs["IAAM.VW.AddItemPopup"] = {
    text           = "Specify the stack size and stack price:",
    button1        = "Add Item",
    button2        = "Cancel",
    OnAccept       = IAAM.VW.AddItemPopup.OnAccept,
    OnShow         = IAAM.VW.AddItemPopup.OnShow,
    timeout        = 0,
    whileDead      = true,
    hideOnEscape   = true,
  --preferredIndex = 3,
    hasEditBox     = false,
    hasItemFrame   = true,
    hasMoneyFrame  = true,
    height         = 200
}